## Source Code for Hopkins TB Mac Model 
Here is the raw source code for the Hopkins TB Mac Model. We apologize that it is quite messy but thought it would be better to make messy code public than to keep it locked away on our computers. If you have any questions please feel free to contact Andrew Azman (azman@jhu.edu) or David Dowdy (ddowdy1@jhmi.edu).
 