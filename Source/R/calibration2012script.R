## ------------------------------------------------------------------- ##
## This is the script file we use to get our model to meet calibration ##
## targets for the TB model comaprison project                         ##
## ------------------------------------------------------------------- ##
## Last update: 1 December 2014

source("Source/R/utility-functions.R")
reload.source()

## read in starting params
params <- make.aug.params()

## make change matrix
chg <- make.chg.mat(params)

## starting state
N <- 36824e3 # 2012 population size
state.start <- numeric(nrow(chg))
names(state.start) <- rownames(chg)
## first put everyone in susceptibles with 80% in low access to care
state.start["S.h0.d0.p0.a0"] <- N*.8-2
state.start["S.h0.d0.p0.a1"] <- N*.2-2
state.start["S.kids"] <- 100000*.8
state.start["L.kids.d0"] <- 100000*.2

## now 1 infected case in each with and without DR
state.start["E.h0.d0.p0.a0"] <- 1
state.start["E.h0.d0.p0.a1"] <- 1
state.start["E.h0.d1.p0.a0"] <- 1
state.start["E.h0.d1.p0.a1"] <- 1

# run the model.frame
params <- make.aug.params()
params['beta_annual_change'] <- 0
params['art_scaleup'] <- 0
params['mu_g'] <- 0
## load older baseline file (doing this post-hoc)
run0 <- ode(y=state.start,times=seq(0,1000,by=1),func=tb.dx.dt,parms=params,chg=chg)
#saveRDS(run0,file="GeneratedData/helpersteadystate.rds")
tmp <- readRDS(file="GeneratedData/helpersteadystate.rds")
baseline <- tail(tmp,1)[,-1]
run0 <- ode(y=baseline,times=seq(0,100,by=1),func=tb.dx.dt,parms=params,chg=make.chg.mat(params))
plot.stats(run0,params)
final.stats(run0,params)

run1 <- ode(y=tail(run0,1)[,-1],
            times=seq(0,100,by=1),
            func=tb.dx.dt,
            parms=params,
            chg=make.chg.mat(params))

## running this over and over to tune
params <- make.aug.params()
params['beta_annual_change'] <- 0
params['art_scaleup'] <- 0
params['mu_g'] <- 0

#tmp.start <- c(tail(run1,1)[,-1])
#tmp.start[c("S.kids","L.kids.d0","L.kids.d1")] <- c(10e6*.6,10e6*.4*.98,10e6*.4*.02)
run1 <- ode(y=tail(run1,1)[,-1],
            times=seq(0,1000,by=1),
            func=tb.dx.dt,
            parms=params,
            chg=make.chg.mat(params))

plot.stats(run1,params)
final.stats(run1,params)
econ.outputs(run1,params,art.adhere = 0.75)

run.2012 <- tail(run1,1)[,-1]

params <- make.aug.params(baseline=TRUE)
run1 <- ode(y=run.2012,
            times=seq(0,1,by=.1),
            func=tb.dx.dt,
            parms=params,
            chg=make.chg.mat(params))

final.stats(run1[1:2,],params)

## going to first tune ART coverage to match future trajectory
## then if we don't have a suitable decrease in ART coverage
## decrease beta to give us addtional decline

## read in starting params
params <- make.aug.params(baseline=FALSE)
params['beta_annual_change'] <- 0
## make change matrix
chg <- make.chg.mat(params)
(artscaleup.dec <- optimize(ob.func.artscaleup,
                            interval = c(0,2),
                            params=params,
                            run.base=run.2012))
params['art_scaleup']<-artscaleup.dec$minimum #0.09406

## just to check
art.up <- ode(y=tail(run1,1)[,-1],
              times=seq(0,23,by=1),
              func=tb.dx.dt,
              parms=params,
              chg=make.chg.mat(params))

art.stats <- get.summary.stats(art.up,params)
## NOTE: fitting this led to a rate that overshoot coverage in later
## years to unreasble levels


## changed beta rate to  -.03 before doing this (from 0)
## and popualtion growth rate
params['beta_annual_change'] <- 0

## # figure out what beta decrease we need to get to 3% decline
## WARNING: I ended up doing this manually (0.01)
(beta.dec <- optimize(ob.fun.2012.inc,
                      interval = c(-1.5,0),
                      state=run.2012,
                      inc.2012=1113)$minimum)
params['beta_annual_change'] <- beta.dec # -0.01

run2 <- ode(y=run.2012,
            times=seq(2012,2013,by=.1),
            func=tb.dx.dt,
            parms=params,
            chg=make.chg.mat(params),
            t.start=2012)
##continue.run(run1,params,extra.t=1,by=1)
make.tbmac.stats(run2,params)

plot.stats(run2,params)
final.stats(run2,params)
## save baseline for start of 2012
saveRDS(run.2012,file="GeneratedData/baseline2012.rds")
saveRDS(tail(run2,1)[,-1],file="GeneratedData/baseline2013.rds")
run.2012 <- readRDS(file="GeneratedData/baseline2012.rds")
